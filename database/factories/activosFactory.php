<?php

namespace Database\Factories;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\datempre;
use App\Models\act_tipos;
use App\Models\act_funciones;
use App\Models\act_estados;

use Carbon\Carbon;

class activosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'terminal' => datempre::all()->random()->cla_empre,
            'descrip' => $this->faker->text(50),
            'serie' =>strtoupper($this->faker->regexify('[A-Za-z]{3}')) . '-' . $this->faker->randomNumber(6),
            'caract' => $this->faker->text(80),
            'id_tipo' => act_tipos::all()->random()->id,
            'id_funcion' => act_funciones::all()->random()->id,
            'id_estado' => act_estados::all()->random()->id,
            'fecha_adq' => Carbon::now()->subYears(rand(1, 9))->subMonths(rand(1, 12))->subDays(rand(1,28))->format('Y-m-d'),
            'responsable' => $this->faker->name(),
            'status' => rand(0,1)
        ];
    }
}
