<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\categorias;
use App\Models\datempre;

use App\Models\act_estados;
use App\Models\act_tipos;
use App\Models\act_funciones;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    public function run()
    {
        categorias::create(['categoria' => 'Ciberseguridad (correo y antivirus)']);
        categorias::create(['categoria' => 'Copias de seguridad']);
        categorias::create(['categoria' => 'Gestión de cuentas de usuario']);
        categorias::create(['categoria' => 'Hosting y computación en la nube']);
        categorias::create(['categoria' => 'Mantenimiento de aplicaciones']);
        categorias::create(['categoria' => 'Mantenimiento de bases de datos']);
        categorias::create(['categoria' => 'Mantenimiento de servidores']);
        categorias::create(['categoria' => 'Mantenimiento y reparación de Impresoras']);
        categorias::create(['categoria' => 'Mantenimiento y reparación de PCs']);
        categorias::create(['categoria' => 'Redes, Telefonía y cableado ']);

        datempre::create(['cla_empre' => '001','descrip' => 'CENTRAL DE AUTOBUSES ACAMBARO',]);
        datempre::create(['cla_empre' => '002','descrip' => 'CENTRAL AMEALCENSE DE AUTOBUSES',]);
        datempre::create(['cla_empre' => '003','descrip' => 'CENTRAL DE AUTOBUSES DE CADEREYTA',]);
        datempre::create(['cla_empre' => '004','descrip' => 'CENTRAL DE AUTOBUSES TRESGUERRAS',]);
        datempre::create(['cla_empre' => '005','descrip' => 'CENTRAL DE AUTOBUSES CORTAZAR',]);
        datempre::create(['cla_empre' => '006','descrip' => 'CENTRAL DE AUTOBUSES SAN FELIPE GUANAJUATO',]);
        datempre::create(['cla_empre' => '007','descrip' => 'ARRENDAMIENTO ALTE&A',]);
        datempre::create(['cla_empre' => '008','descrip' => 'CENTRAL DE AUTOBUSES GUANAJUATO',]);
        datempre::create(['cla_empre' => '009','descrip' => 'CENTRAL DE AUTOBUSES IRAPUATO',]);
        datempre::create(['cla_empre' => '010','descrip' => 'TERMINAL DE AUTOBUSES DE JALPAN',]);
        datempre::create(['cla_empre' => '011','descrip' => 'CENTRAL DE AUTOBUSES LA BARCA',]);
        datempre::create(['cla_empre' => '012','descrip' => 'CENTRAL DE AUTOBUSES DE LA PIEDAD',]);
        datempre::create(['cla_empre' => '013','descrip' => 'ESTACION CENTRAL DE AUTOBUSES LEON DE LOS ALDAMAS',]);
        datempre::create(['cla_empre' => '014','descrip' => 'TERMINAL DE AUTOBUSES MOROLEON',]);
        datempre::create(['cla_empre' => '015','descrip' => 'CENTRAL DE AUTOBUSES OCOTLAN, JALISCO',]);
        datempre::create(['cla_empre' => '016','descrip' => 'TERMINAL DE AUTOBUSES DE QUERETARO',]);
        datempre::create(['cla_empre' => '017','descrip' => 'CENTRAL DE AUTOBUSES DE SALAMANCA',]);
        datempre::create(['cla_empre' => '018','descrip' => 'CENTRAL DE AUTOBUSES SALVATIERRA',]);
        datempre::create(['cla_empre' => '019','descrip' => 'CENTRAL DE AUTOBUSES DE SAN JUAN DEL RIO',]);
        datempre::create(['cla_empre' => '020','descrip' => 'ESTACION CENTRAL DE AUTOBUSES LEON DE LOS ALDAMAS',]);
        datempre::create(['cla_empre' => '021','descrip' => 'CENTRAL DE AUTOBUSES SAN FELIPE GUANAJUATO',]);
        datempre::create(['cla_empre' => '022','descrip' => 'TERMINAL TERRESTRE POTOSINA S.A. DE C.V.',]);
        datempre::create(['cla_empre' => '023','descrip' => 'CENTRAL DE AUTOBUSES SAN LUIS DE LA PAZ',]);
        datempre::create(['cla_empre' => '024','descrip' => 'CENTRAL DE AUTOBUSES SAN MIGUEL DE ALLENDE',]);
        datempre::create(['cla_empre' => '025','descrip' => 'TERMINAL DE AUTOBUSES TEQUISQUIAPAN',]);
        datempre::create(['cla_empre' => '026','descrip' => 'TERMINAL DE AUTOBUSES URIANGATO **',]);
        datempre::create(['cla_empre' => '027','descrip' => 'TERMINAL AUTOBUSES MANZANILLO',]);
        datempre::create(['cla_empre' => '028','descrip' => 'TERMINAL AUTOBUSES ZAPOTLAN',]);
        datempre::create(['cla_empre' => '037','descrip' => 'CENTRAL CAMIONERA NUEVO MILENIO DE GUADALAJARA',]);
        datempre::create(['cla_empre' => '038','descrip' => 'CENTRAL DE AUTOBUSES CORTAZAR',]);
        datempre::create(['cla_empre' => '039','descrip' => 'CENTRAL DE AUTOBUSES CORTAZAR',]);
        datempre::create(['cla_empre' => '040','descrip' => 'CENTRAL DE AUTOBUSES CORTAZAR',]);
        datempre::create(['cla_empre' => '041','descrip' => 'TERMINAL COSTERA GUAYABITOS',]);

        act_estados::create(['estado' => 'Activos en uso']);
        act_estados::create(['estado' => 'Activos en reserva']);
        act_estados::create(['estado' => 'Activos obsoletos']);
        act_estados::create(['estado' => 'Activos reparac./mantenim.']);

        act_tipos::create(['tipo' => 'Computadora de escritorio']);
        act_tipos::create(['tipo' => 'Laptops (portatiles)']);
        act_tipos::create(['tipo' => 'Servidores']);
        act_tipos::create(['tipo' => 'Dispositivos móviles']);
        act_tipos::create(['tipo' => 'Perifericos']);
        act_tipos::create(['tipo' => 'Redes']);

        act_funciones::create(['funcion' => 'Equip. de producción']);
        act_funciones::create(['funcion' => 'Equip. de comunicación']);
        act_funciones::create(['funcion' => 'Equip. perifericos']);
        act_funciones::create(['funcion' => 'Equip. de red']);


        $this -> call(RoleSeeder::class);
        //$this -> call(DatosPruebaSeeder::class);

        // CREAR USUARIOS
        User::factory()->create([
            'name' => 'Alfonso CH R',
            'email' => 'alfonso.chavez.rgz@gmail.com',
            'password' => bcrypt('12345678'),
        ])->assignRole('Admin');

    }
}
