<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activos', function (Blueprint $table) {
            $table->id();
            $table->string('terminal', 4);
            $table->longText('descrip');
            $table->string('serie', 100);
            $table->longText('caract')->nullable();
            $table->foreignId('id_estado');
            $table->date('fecha_adq');
            $table->string('responsable', 100)->nullable();
            $table->foreignId('id_tipo');
            $table->foreignId('id_funcion');
            $table->foreignId('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activos');
    }
}
