<?php

use App\Http\Controllers\ticketController;
use App\Http\Controllers\categoriaController;
use App\Http\Controllers\UserSettingsController;
use App\Http\Controllers\usuarioController;
use App\Http\Controllers\roleController;
use App\Http\Controllers\reportesController;

use App\Http\Controllers\activoController;
use App\Http\Controllers\act_tipoController;
use App\Http\Controllers\act_funcionController;
use App\Http\Controllers\act_estadoController;
use App\Http\Controllers\reportesAController;


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// == HOME ==
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('can:home')->name('home');

// == TICKET ==
Route::resource('ticket',ticketController::class)->names('ticket');
// == CATEGORIA ==
Route::resource('categoria',categoriaController::class)->names('categoria');
// == USUARIOS ==
Route::resource('usuario',usuarioController::class)->names('usuario');
// == ROLES ==
Route::resource('role',roleController::class)->names('role');
// == REPORTES ==
Route::resource('reportes',reportesController::class)->names('reportes');
// Ruta personalizada para TICKET PROCESAR
Route::post('ticket/procesar', [TicketController::class, 'procesar'])->name('ticket.procesar');


Auth::routes();
Route::get('/profile', [UserSettingsController::class, 'Profile'])->name('Profile')->middleware('auth');
Route::post('/change/profile', [UserSettingsController::class, 'changeProfile'])->name('changeProfile');


// == ACTIVOS ==
Route::resource('activo',activoController::class)->names('activo');
// == TIPOS ==
Route::resource('tipo',act_tipoController::class)->names('tipo');
// == FUNCIONES ==
Route::resource('funcion',act_funcionController::class)->names('funcion');
// == ESTADOS ==
Route::resource('estado',act_estadoController::class)->names('estado');
// == REPORTE ACTIVOS ==
Route::resource('reportesA',reportesAController::class)->names('reportesA');
// Ruta personalizada para ACTIVO PROCESAR
Route::post('activo/procesar', [activoController::class, 'procesar'])->name('activo.procesar');
