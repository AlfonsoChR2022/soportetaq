<table style="margin: 0; auto; width:100%">
    <tr>
        <td>
            <div class="form-group" >
                {!! Form::label('tipo', 'Tipo de Equipo:') !!}
                {!! Form::text('tipo',null,['class' => 'form-control','placeholder' => 'Ingrese el nombre del Equipo']) !!}
                @error('tipo')<small class="text-danger">* {{ $message}}</small><br> @enderror
            </div>
        </td>
    </tr>
</table>