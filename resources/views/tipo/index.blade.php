@extends('adminlte::page')

@section('title', 'Tipos de Equipo')

@section('content_header')
    @can('tipo.create')
        <a href="{{route('tipo.create')}}" style = "height: 40px;" class="btn-info rounded text-white px-3 py-2 text-base float-right">Nuevo Tipo de Equipo</a>
    @endcan
    <h1>Listado de Tipos de Equipo</h1>
@stop

@section('content')
    @if (session('info'))
            <div class="alert alert-success">
                <strong>{{ session('info') }}</strong>
            </div>
        @endif
     <div class="card">
        @if ($tips->count())
        <div class="card-body">
            <table class="table table-striped text-sm"   style="margin: 0; auto;" cellspacing="0" cellpadding="0">
                <thead style="padding:1px; background-color:#5f2167; color:white">
                        <th >#</th>
                        <th >Tipo</th>
                        <th colspan="2"></th>
                </thead>
                <tbody>
                    @foreach ($tips as $tipo)
                        <tr>
                            <td> {{ $tipo -> id }} </td>
                            <td> {{ $tipo -> tipo }} </td>
                            <td width="10px">
                                @can('tipo.show')
                                    <a href= "{{route('tipo.show',$tipo->id)}}" style="width:150px" class="btn btn-primary">Ver Tipo</a>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer" >
            {{ $tips->links() }}
        </div>
        <div>
            @push('scripts')
                <script>
                    Livewire.restart();
                </script>
            @endpush
        </div>
        @else
            <strong>No hay ningun Tipo...</strong>
        @endif
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/body.css') }}">
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
    <style>
        .sidebar-dark-primary{
            background: #5f2167 !important;
            }
        .brand-link{
            background: #5f2167 !important;
            }
    </style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop