@extends('adminlte::page')

@section('title', 'Ver Activo')


@section('content_header')
<div id="miReporte" class="container-fluid" style="width:100%;">
    <div class="row">
        <div class="co w-75">
            <h2 style="color:#5f2167">
                ACTIVO #{{$activo-> id}}
            </h2>
        </div>
        <div class="col align-self-end w-75">
            <div class="d-flex justify-content-end m-0 p-0 align-self-end co" >
                <button onclick="imprimirTabla()" class="btn btn-default mr-2"><i class="fas fa-print"></i>  Imprimir</button>
                <a href= "{{route('reportesA.index')}}" class="btn-info rounded text-white px-3 py-2 mx-1 text-base float-right">Volver
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="d-block p-0 align-bottom text-base" style="font-size:12px;font-weight: normal;">
            ({{ $terminal->cla_empre }}) {{ $terminal->descrip }}
        </div>
    </div>
</div>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif

    <div class="card" id="miReporte2">
        <div class="card-body">

            <div class="container-fluid" style="width:100%;">
                <div class="row" >
                    <div class="col align-self-start">
                        <h3 class="text-info">
                            {{$activo -> descrip}}
                        </h3>
                    </div>

                    <div class="col align-self-end">
                        <div class="d-flex align-items-end justify-content-end m-0 p-0 align-self-end" >
                            <span class="d-block p-0 align-bottom text-base" style="font-size:12px;font-weight: normal;">ESTADO: </span>
                                @switch($activo -> status)
                                    @case(1)
                                        <span class="d-block p-0 text-success">ACTIVO</span>
                                        @break
                                    @case(0)
                                        <span class="d-block p-0 text-danger">INACTIVO</span>
                                        @break
                                    @default
                                        <span class="d-block p-0">---</span>
                                        @break
                                @endswitch
                        </div>
                    </div>

                    
                </div>

                <div class="row">
                    <div class="col align-self-start">
                        <div class="d-flex align-items-end justify-content-start m-0 p-0 ">
                            <span class="d-block p-0 align-bottom text-base" style="font-size:12px;font-weight: normal;">Responsable:</span>
                            {{$activo->responsable}}
                        </div>
                    </div>
                    <div class="col align-self-end">
                        <div class="d-flex align-items-end justify-content-end m-0 p-0 align-self-end" >
                            <span class="d-block p-0 align-bottom text-base" style="font-size:12px;font-weight: normal;">Fecha Adquisición: </span>
                            {{ \Carbon\Carbon::parse($activo->fecha_adq)->format('d/m/Y') }}
                        </div>
                    </div>
                </div>

            </div>

            <hr class="mt-1 mb-1"/>

            <br>
            <div class="container-fluid" style="width:100%;">
                <div class="row">
                    <div class="co w-50">
                        <div class="stl_body">
                            <strong>Tipo de Equipo</strong><br>
                            <span style="border-bottom: 1px solid lightgray; padding: 5px; display: inline-block; width: 90%;">
                                {{$tipo}}
                            </span>
                        </div>
                    </div>

                    <div class="co w-50">
                        <div class="stl_body">
                            <strong>Función</strong><br>
                            <span style="border-bottom: 1px solid lightgray; padding: 5px; display: inline-block; width: 90%;">
                                {{ $funcion }}
                            </span>
                        </div>
                    </div>

                </div>
            </div>

            <br>
            <div class="container-fluid" style="width:100%;">
                <div class="row">

                    <div class="co w-50">
                        <div class="stl_body">
                            <strong>Estado</strong><br>
                            <span style="border-bottom: 1px solid lightgray; padding: 5px; display: inline-block; width: 90%;">
                                {{ $estado}}
                            </span>
                        </div>
                    </div>

                    <div class="co w-50">
                        <div class="stl_body">
                            <strong>Serie</strong><br>
                            <span style="border-bottom: 1px solid lightgray; padding: 5px; display: inline-block; width: 90%;">
                                {{$activo-> serie}}
                            </span>
                        </div>
                    </div>

                </div>
            </div>

            <br>
            <div class="row" >
                <div class="col">
                    <div class="stl_body">
                            <strong>Caracteristicas</strong><br>
                            <div style="border-bottom: 1px solid lightgray; border-left: 1px solid lightgray; padding: 5px; display: inline-block;height:100px; width: 95%;">
                                {!!$activo->caract!!}
                            </div>
                    </div>
                </div>
            </div>

        <br>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/body.css') }}">
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
    <style>
        .sidebar-dark-primary{
            background: #5f2167 !important;
            }
        .brand-link{
            background: #5f2167 !important;
            }
    </style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script>
        function imprimirTabla() {
          var tabla = document.getElementById('miReporte').outerHTML;
          var tabla2 = document.getElementById('miReporte2').outerHTML;

          var ventana = window.open('', '', 'height=500,width=800');
          ventana.document.write('<html><head><title>Reporte</title>');
          ventana.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">');
          ventana.document.write('</head><body>');
          ventana.document.write(tabla);
          ventana.document.write('<br>');
          ventana.document.write(tabla2);
          ventana.document.write('</body></html>');
          ventana.document.close();
    
        // Añadir un retraso de 1 segundo antes de imprimir
        setTimeout(function() {
            ventana.print();
          }, 250); // 1000 milisegundos = 1 segundo
        }
    </script>
@stop