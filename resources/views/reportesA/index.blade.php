@extends('adminlte::page')

@section('title', 'Reporte Activos')

@section('content_header')
<h1 id="miReporte">Reporte de Activos</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {{-- @livewire('activos.reporte-activo-component') --}}
        @livewire('activos.reporte-activo-component')
        <br><br>
    </div>
</div>
@stop


@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/body.css') }}">
    <style>
        .sidebar-dark-primary{
            background: #5f2167 !important;
            }

        .brand-link{
            background: #5f2167 !important;
            }
            
        /* .navbar{background: #5f2167 !important;} */

    </style>
@stop

@section('js')
    {{-- <script> console.log('Hi!'); </script> --}}
    {{-- @stack('modals') --}}
    @livewireScripts
    {{-- @vite(['resources/css/app.css', 'resources/js/app.js']) --}}

    <script>


        function imprimirTabla() {
          var tabla = document.getElementById('miReporte').outerHTML;
          var tabla2 = document.getElementById('miReporte2').outerHTML;

          var varSearch = document.getElementById('search').value;
          var comboTerminalFiltro = document.getElementById('terminalFiltro').options[document.getElementById('terminalFiltro').selectedIndex].text;
          var comboTipoFiltro = document.getElementById('tipoFiltro').options[document.getElementById('tipoFiltro').selectedIndex].text;
          var comboFuncionFiltro = document.getElementById('funcionFiltro').options[document.getElementById('funcionFiltro').selectedIndex].text;
          var comboEstadoFiltro = document.getElementById('estadoFiltro').options[document.getElementById('estadoFiltro').selectedIndex].text;

          var ventana = window.open('', '', 'height=500,width=800');
          ventana.document.write('<html><head><title>Reporte</title>');
          ventana.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">');
          ventana.document.write('</head><body>');
          ventana.document.write(tabla + '<br>');

          ventana.document.write('<strong>FILTROS</strong><br>');
          if (varSearch === null || varSearch === '') {}else{ventana.document.write('Palabra a buscar: ' + varSearch + '<br>');}
          ventana.document.write('Terminal: ' + comboTerminalFiltro + '<br>');
          ventana.document.write('Tipo: ' + comboTipoFiltro + '<br>');
          ventana.document.write('Función: ' + comboFuncionFiltro + '<br>');
          ventana.document.write('Estado: ' + comboEstadoFiltro + '<br>');
          ventana.document.write('<br>');

          ventana.document.write(tabla2);
          ventana.document.write('</body></html>');
          ventana.document.close();
    
        // Añadir un retraso de 1 segundo antes de imprimir
        setTimeout(function() {
            ventana.print();
          }, 280); // 1000 milisegundos = 1 segundo
        }
    </script>
@stop