@extends('adminlte::page')

@section('title', 'Soporte TAQ')


@section('content_header')
    <h1>Bienvenido</h1>
@stop

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">PANEL INFORMATIVO</div><br>
                <div class="card-body">


                    <h6>TICKET'S</h6>
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3>{{ $concentrado->where('status', '=', 1)->first()->score ?? 0 }}</h3>
                                    <p>En Espera</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-hourglass"></i>
                                </div>
                                    <form id="myForm_1" action="{{ route('ticket.procesar') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="statusFiltro" value="1">
                                    </form>
                                    <a href="#" onclick="document.getElementById('myForm_1').submit(); return false;" class="small-box-footer">
                                        Click para ver <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>{{ $concentrado->where('status', '=', 2)->first()->score ?? 0 }}</h3>
                                    <p>En Proceso</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-wrench"></i>
                                </div>
                                <form id="myForm_2" action="{{ route('ticket.procesar') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="statusFiltro" value="2">
                                </form>
                                <a href="#" onclick="document.getElementById('myForm_2').submit(); return false;" class="small-box-footer">
                                    Click para ver <i class="fas fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{ $concentrado->where('status', '=', 3)->first()->score ?? 0 }}</h3>
                                    <p>Terminados</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                <form id="myForm_3" action="{{ route('ticket.procesar') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="statusFiltro" value="3">
                                </form>
                                <a href="#" onclick="document.getElementById('myForm_3').submit(); return false;" class="small-box-footer">
                                    Click para ver <i class="fas fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>{{ $concentrado->where('status', '=', 4)->first()->score ?? 0}}</h3>
                                    <p>Cancelados</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-times-circle"></i>
                                </div>
                                <form id="myForm_4" action="{{ route('ticket.procesar') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="statusFiltro" value="4">
                                </form>
                                <a href="#" onclick="document.getElementById('myForm_4').submit(); return false;" class="small-box-footer">
                                    Click para ver <i class="fas fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <h6>ACTIVOS</h6>
                    <div class="row">

                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-olive">
                                <div class="inner">
                                    <h3>{{ $concentradoA->where('status', '=', '1')->first()->score ?? 0 }}</h3>
                                    <p>Activos</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-check"></i>
                                </div>
                                    <form id="myForm_11" action="{{ route('activo.procesar') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="statusFiltro" value="1">
                                    </form>
                                    <a href="#" onclick="document.getElementById('myForm_11').submit(); return false;" class="small-box-footer">
                                        Click para ver <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-6">
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>{{ $concentradoA->where('status', '=', '0')->first()->score ?? 0 }}</h3>
                                    <p>Inactivos</p>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-times-circle"></i>
                                </div>
                                <form id="myForm_12" action="{{ route('activo.procesar') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="statusFiltro" value="0">
                                </form>
                                <a href="#" onclick="document.getElementById('myForm_12').submit(); return false;" class="small-box-footer">
                                    Click para ver <i class="fas fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                <br><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/body.css') }}">

    <style>
        .sidebar-dark-primary{
            background: #5f2167 !important;
            }
        .brand-link{
            background: #5f2167 !important;
            }
    </style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
