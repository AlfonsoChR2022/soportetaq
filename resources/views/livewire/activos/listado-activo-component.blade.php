<div>
        <div style="display: flex; justify-content: space-between;">
            <div style="width: calc(50%); margin: 3px; display: flex; align-items: flex-end;">
                <strong>Filtrar:</strong>
            </div>
            <div style="width: calc(50%); margin: 3px; display: flex; justify-content: flex-end;  ">
                <button wire:click="seleccionarPrimerElemento" id="seleccionarPrimerElementoBtn" class="btn btn-default"><i class="fas fa-sync-alt"></i></button>
            </div>
        </div>

        <div style="display: flex; flex-wrap: wrap; background-color:#5f2167;color: #ffffff;width: 100%;" >
            <div style="width: 200px; margin: 10px;">
                Equipo
                <input type="text" wire:model="search" placeholder="Buscar equipo" class="form-control">
            </div>

            <div style="width: 200px; margin: 10px;">
                Terminales
                <select wire:model="terminalFiltro" class="form-control" >
                    <option value="">Todas</option>
                    @foreach($terminales as $terminal)
                        <option value="{{ $terminal['cla_empre'] }}">({{ $terminal['cla_empre'] }}) {{ $terminal['descrip'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Tipo de equipo
                <select wire:model="tipoFiltro" class="form-control" >
                    <option value="">Todos</option>
                    @foreach($t_tipo as $tipo)
                        <option value="{{ $tipo['id'] }}">{{ $tipo['tipo'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Función
                <select wire:model="funcionFiltro" class="form-control" >
                    <option value="">Todas</option>
                    @foreach($t_funcion as $funcion)
                        <option value="{{ $funcion['id'] }}">{{ $funcion['funcion'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Estado
                <select wire:model="estadoFiltro" class="form-control" >
                    <option value="">Todas</option>
                    @foreach($t_estado as $estado)
                        <option value="{{ $estado['id'] }}">{{ $estado['estado'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Status
                <select wire:model="statusFiltro" class="form-control" >
                    <option value="">Todas</option>
                    <option value="1">Activo</option>
                    <option value="0">Inactivo</option>
                </select>
            </div>
    </div>

@if($activos->count())
    <table class="table table-striped text-sm"   style="margin: 0; auto;" cellspacing="0" cellpadding="0">
        <thead style="padding:1px; background-color:#5f2167; color:white">
            <th class="cursor-pointer" width="20px">#
            </th>
            <th class="cursor-pointer" width="10px">Terminal
            </th>
            <th  class="cursor-pointer" width="100px">Descripción
            </th>
            <th  class="cursor-pointer" width="100px">Serie
            </th>
            <th class="cursor-pointer" width="100px">Tipo
            </th>
            <th class="cursor-pointer" width="150px">Función
            </th>
            <th class="cursor-pointer" width="150px">Estado
            </th>
            <th class="cursor-pointer" width="150px">Fecha Adqui.
            </th>
            <th class="cursor-pointer" width="150px">Status
            </th>
            <th width="10px"></th>
        </thead>
        <tbody>
            @foreach ($activos as $actv)
                <tr>
                    <td> {{ $actv -> id }} </td>
                    <td> ({{ $actv -> terminal }}) </td>
                    <td> {{ $actv -> descrip }} </td>
                    <td> {{ $actv -> serie }} </td>
                    <td>
                        @foreach( $t_tipo->where('id', $actv->id_tipo) as $algo )
                            {{ $algo -> tipo }}
                        @endforeach
                    </td>
                    <td>
                        @foreach( $t_funcion->where('id', $actv->id_funcion) as $algo )
                            {{ $algo -> funcion }}
                        @endforeach
                    </td>
                    <td>
                        @foreach( $t_estado->where('id', $actv->id_estado) as $algo )
                            {{ $algo -> estado }}
                        @endforeach
                    </td>
                    <td> {{ $actv -> fecha_adq }} </td>
                    <td>
                        @switch($actv -> status)
                            @case(0)
                                <p class="text-danger">Inactivo</p>
                                @break
                            @case(1)
                                <p class="text-success">Activo</p>
                                @break
                            @default
                                <p>---</p>
                                @break
                        @endswitch
                    </td>
                    <td>
                        @can('activo.show')
                                    <a href= "{{route('activo.show',$actv -> id )}}" style="width:100px" class="btn btn-primary">Ver activo</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="card-footer" >
        {{ $activos->links() }}
    </div>

    <div>
        @push('scripts')
            <script>
                Livewire.restart();
            </script>
        @endpush
    </div>

@else
    <div class="card-header">
        <br>
        <strong>No se encontraron coincidencias con tus parámetros de búsqueda.!</strong>
        <br><br>
    </div>
@endif
</div>

