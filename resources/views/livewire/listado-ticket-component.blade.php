<div>
    {{-- Searh: {{  $search }} <br>
    Count: {{ $tkts->count() }} --}}
        <div style="display: flex; justify-content: space-between;">
            <div style="width: calc(50%); margin: 3px; display: flex; align-items: flex-end;">
                <strong>Filtrar:</strong>
            </div>
            <div style="width: calc(50%); margin: 3px; display: flex; justify-content: flex-end;  ">
                <button wire:click="seleccionarPrimerElemento" id="seleccionarPrimerElementoBtn" class="btn btn-default"><i class="fas fa-sync-alt"></i></button>
            </div>
        </div>

        <div style="display: flex; flex-wrap: wrap; background-color:#5f2167;color: #ffffff;" >
            <div style="width: 200px; margin: 10px;">
                Evento
                <input type="text" wire:model="search" placeholder="Buscar evento" class="form-control" placeholder="Buscar evento">
            </div>

            <div style="width: 200px; margin: 10px;">
                Terminales
                <select wire:model="terminalFiltro" class="form-control" >
                    <option value="">Todas</option>
                    @foreach($terminales as $terminal)
                        <option value="{{ $terminal['cla_empre'] }}">({{ $terminal['cla_empre'] }}) {{ $terminal['descrip'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Categorias
                <select wire:model="categoriaFiltro" class="form-control" >
                    <option value="">Todas</option>
                    @foreach($t_evto as $categoria)
                        <option value="{{ $categoria['id'] }}">{{ $categoria['categoria'] }}</option>
                    @endforeach
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Prioridad
                <select wire:model="prioridadFiltro" class="form-control" >
                    <option value="">Todas</option>
                    <option value="1">Alta</option>
                    <option value="2">Media</option>
                    <option value="3">Baja</option>
                </select>
            </div>

            <div style="width: 200px; margin: 10px;">
                Status
                <select wire:model="statusFiltro" class="form-control" >
                    <option value="">Todos</option>
                    <option value="1">En espera</option>
                    <option value="2">En proceso</option>
                    <option value="3">Terminado</option>
                    <option value="4">Cancelado</option>
                </select>
            </div>
    </div>

@if($tkts->count())
    <table class="table table-striped text-sm"   style="margin: 0; auto;" cellspacing="0" cellpadding="0">
        <thead style="padding:1px; background-color:#5f2167; color:white">
            <th class="cursor-pointer" width="20px">#
            </th>
            <th class="cursor-pointer" width="10px">Terminal
            </th>
            <th  class="cursor-pointer" width="100px">Categoria
            </th>
            <th class="cursor-pointer" width="150px">Evento
            </th>
            <th class="cursor-pointer" width="100px">Prioridad
            </th>
            <th class="cursor-pointer" width="150px">Fecha Creación
            </th>
            <th class="cursor-pointer" width="150px">Fecha Cierre
            </th>
            <th class="cursor-pointer" width="150px">Status
            </th>
            <th class="cursor-pointer" width="150px">Solicita
            </th>
            <th class="cursor-pointer" width="150px">Atiende
            </th>
            <th width="10px"></th>
        </thead>
        <tbody>
            @foreach ($tkts as $ticket)
                <tr>
                    <td> {{ $ticket -> id }} </td>
                    <td> ({{ $ticket -> terminal }}) </td>
                    <td>
                        @foreach( $t_evto->where('id', $ticket -> id_categoria) as $algo )
                            {{ $algo -> categoria }}
                        @endforeach
                    </td>
                    <td> {{ $ticket -> evento }} </td>
                    <td>
                        @switch($ticket->prioridad)
                            @case(1)
                                <p class="text-danger">Alta</p>
                                @break
                            @case(2)
                                <p class="text-warning">Media</p>
                                @break
                            @case(3)
                                <p class="text-success">Baja</p>
                                @break
                            @default
                                <p>---</p>
                                @break
                        @endswitch
                    </td>
                    <td> {{ $ticket -> fecha_crea }} </td>
                    <td> {{ $ticket -> fecha_cierre }} </td>
                    <td>
                        @switch($ticket -> status)
                            @case(1)
                                <p class="text-success">En espera</p>
                                @break
                            @case(2)
                                <p class="text-warning">En proceso</p>
                                @break
                            @case(3)
                                <p class="text-primary">Terminado</p>
                                @break
                            @case(4)
                                <p class="text-danger">Cancelado</p>
                                @break
                            @default
                                <p>---</p>
                                @break
                        @endswitch
                    </td>
                    <td>
                        @foreach( $userX->where('id', $ticket -> user) as $algo2 )
                            {{ $algo2 -> name }}
                        @endforeach
                    </td>
                    <td>
                        @foreach( $userX->where('id', $ticket -> atiende) as $algo3 )
                            {{ $algo3 -> name }}
                        @endforeach
                    </td>
                    <td>
                        @can('ticket.show')
                                    <a href= "{{route('ticket.show',$ticket -> id )}}" style="width:100px" class="btn btn-primary">Ver Ticket</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="card-footer" >
        {{ $tkts->links() }}
    </div>

    <div>
        @push('scripts')
            <script>
                Livewire.restart();
            </script>
        @endpush
    </div>

@else
    <div class="card-header">
        <br>
        <strong>No se encontraron coincidencias con tus parámetros de búsqueda.!</strong>
        <br><br>
    </div>
@endif
</div>

