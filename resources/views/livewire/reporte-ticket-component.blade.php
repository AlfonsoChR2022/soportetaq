<div>
    {{-- Searh: {{  $search }} <br>
    Count: {{ $tkts->count() }} --}}

    <div style="display: flex; justify-content: space-between;">
        <div style="width: calc(50%); margin: 3px; display: flex; align-items: flex-end;">
            <strong>Filtrar:</strong>
        </div>
        <div style="width: calc(50%); margin: 3px; display: flex; justify-content: flex-end;  ">
            <button onclick="imprimirTabla()" class="btn btn-default mr-2"><i class="fas fa-print"></i></button>
            <button wire:click="seleccionarPrimerElemento" id="seleccionarPrimerElementoBtn" class="btn btn-default mr-2"><i class="fas fa-sync-alt"></i></button>
        </div>
    </div>

    <div style="display: flex; flex-wrap: wrap;" class="bg-info">
        <div style="width: 200px; margin: 10px;">
            Evento
            <input id="search"  type="text" wire:model="search" placeholder="Buscar evento" class="form-control" placeholder="Buscar evento">
        </div>

        <div style="width: 200px; margin: 10px;">
            Terminales
            <select id="terminalFiltro" wire:model="terminalFiltro" class="form-control" >
                <option value="">Todas</option>
                @foreach($terminales as $terminal)
                    <option value="{{ $terminal['cla_empre'] }}">({{ $terminal['cla_empre'] }}) {{ $terminal['descrip'] }}</option>
                @endforeach
            </select>
        </div>

        <div style="width: 200px; margin: 10px;">
            Categorias
            <select  id="categoriaFiltro" wire:model="categoriaFiltro" class="form-control" >
                <option value="">Todas</option>
                @foreach($t_evto as $categoria)
                    <option value="{{ $categoria['id'] }}">{{ $categoria['categoria'] }}</option>
                @endforeach
            </select>
        </div>

        <div style="width: 200px; margin: 10px;">
            Prioridad
            <select  id="prioridadFiltro" wire:model="prioridadFiltro" class="form-control" >
                <option value="">Todas</option>
                <option value="1">Alta</option>
                <option value="2">Media</option>
                <option value="3">Baja</option>
            </select>
        </div>
</div>

@if($tkts->count())
    <table id="miReporte2" class="table table-striped text-sm"   style="margin: 0; auto;" cellspacing="0" cellpadding="0">
        <thead style="padding:1px; color:white" class="bg-info">
            <th wire:click="order('id')" class="cursor-pointer" width="20px">#
                @if ($sort == 'id')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('terminal')" class="cursor-pointer" width="10px">Terminal
                @if ($sort == 'terminal')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('id_categoria')" class="cursor-pointer" width="200px">Categoria
                @if ($sort == 'id_categoria')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('evento')" class="cursor-pointer" width="250px">Evento
                @if ($sort == 'evento')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('prioridad')" class="cursor-pointer" width="100px">Prioridad
                @if ($sort == 'prioridad')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('fecha_crea')" class="cursor-pointer" width="150px">Fecha Creación
                @if ($sort == 'fecha_crea')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th wire:click="order('fecha_cierre')" class="cursor-pointer" width="150px">Fecha Cierre
                @if ($sort == 'fecha_cierre')
                    @if ($direction == 'asc')
                        <i class="fas fa-sort-alpha-up-alt float-right mt-1"></i>
                    @else
                        <i class="fas fa-sort-alpha-down-alt  float-right mt-1"></i>
                    @endif
                @else
                    <i class="fas fa-sort float-right mt-1"></i>
                @endif
            </th>
            <th width="10px"></th>
        </thead>
        <tbody>
            @foreach ($tkts as $ticket)
                <tr>
                    <td> {{ $ticket -> id }} </td>
                    <td> ({{ $ticket -> terminal }}) </td>
                    <td>
                        @foreach( $t_evto->where('id', $ticket -> id_categoria) as $algo )
                            {{ $algo -> categoria }}
                        @endforeach
                    </td>
                    <td> {{ $ticket -> evento }} </td>
                    <td>
                        @switch($ticket->prioridad)
                            @case(1)
                                <p class="text-danger">Alta</p>
                                @break
                            @case(2)
                                <p class="text-warning">Media</p>
                                @break
                            @case(3)
                                <p class="text-success">Baja</p>
                                @break
                            @default
                                <p>---</p>
                                @break
                        @endswitch
                    </td>
                    <td> {{ $ticket -> fecha_crea }} </td>
                    <td> {{ $ticket -> fecha_cierre }} </td>
                    <td>
                        @can('reportes.show')
                            <a href= "{{route('reportes.show',['reporte' => $ticket->id]) }}" style="width:100px" class="btn btn-primary">Ver Ticket</a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="card-footer" >
        {{ $tkts->links() }}
    </div>

    <div>
        @push('scripts')
            <script>
                Livewire.restart();
            </script>
        @endpush
    </div>

@else
    <div id="miReporte2" class="card-header">
        <br>
        <strong>No se encontraron coincidencias con tus parámetros de búsqueda.!</strong>
        <br><br>
    </div>
@endif
</div>

