<table style="margin: 0; auto; width:100%">
    <tr>
        <td>
            <div class="form-group" >
                {!! Form::label('estado', 'Función:') !!}
                {!! Form::text('estado',null,['class' => 'form-control','placeholder' => 'Ingrese el nombre del estado']) !!}
                @error('estado')<small class="text-danger">* {{ $message}}</small><br> @enderror
            </div>
        </td>
    </tr>
</table>