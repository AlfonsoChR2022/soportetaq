@extends('adminlte::page')

@section('title', 'Ver Estados')

@section('content_header')
<div class="container-fluid" style="width:100%;">
    <div class="row">
        <div class="co w-75">
            <h2 style="color:#5f2167">
                Estado #{{$estadorium -> id}}
            </h2>
        </div>
        <div class="col align-self-end w-75">
            <div class="d-flex justify-content-end m-0 p-0 align-self-end co" >
                @can('estado.edit')
                    <a href= "{{route('estado.edit',$estadorium)}}" class="btn-primary rounded text-white px-3 py-2 mx-1 text-base float-right">Editar Estado</a>
                @endcan

                @can('estado.destroy')
                    <form action="{{route('estado.destroy',$estadorium)}}" class="formulario-eliminar-estado" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-danger rounded text-white px-3 py-2 mx-1 text-base float-right">Eliminar Estado</a>
                    </form>
                @endcan
                
                
            </div>
        </div>
    </div>
</div>
@stop

@section('content')
    @if (session('info'))
        <div class="alert alert-success">
            <strong>{{ session('info') }}</strong>
        </div>
    @endif
    
    <div class="card">
        <div class="card-body">

            <div class="row" >
                <div class="col">
                    <div class="stl_body">
                        <div class="row" >
                            <div class="co w-100">
                                <h5 style="color:#5f2167; text-transform: uppercase">
                                    DESCRIPCIÓN
                                </h5>
                            </div>
                        </div>
                        <hr class="mt-1 mb-1"/>

                        <div class="row" >
                            <div class="co w-100">
                                <h5 class="text-info">
                                    {!!$estadorium->estado!!}
                                </h5>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <hr class="mt-1 mb-1"/>

            <div style="display: flex; justify-content: right;">
                <div class="d-flex justify-content-end m-0 p-0 align-self-end">
                        <a href= "{{route('estado.index')}}" class="btn-info rounded text-white px-3 py-1 text-base mx-1 float-rigth align-baseline">Volver</a>
                </div>
            </div>

        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/body.css') }}">
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
    <style>
        .sidebar-dark-primary{
            background: #5f2167 !important;
            }
        .brand-link{
            background: #5f2167 !important;
            }
    </style>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $('.formulario-eliminar-estado').submit(function(e){
            e.preventDefault();
        Swal.fire({
                    title: "¿Estás seguro?",
                    text: "Se eliminará la estado.!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Si, eliminar",
                    cancelButtonText: "Cancelar"
                }).then((result) => {
                    if (result.isConfirmed) {
                        this.submit();
                    } else{
                        return false;
                    }
                });
        });
    </script>
@stop