<table style="margin: 0; auto; width:100%">
    <tr>
        <td>
            <div class="form-group" >
                {!! Form::label('funcion', 'Función:') !!}
                {!! Form::text('funcion',null,['class' => 'form-control','placeholder' => 'Ingrese el nombre de función']) !!}
                @error('funcion')<small class="text-danger">* {{ $message}}</small><br> @enderror
            </div>
        </td>
    </tr>
</table>