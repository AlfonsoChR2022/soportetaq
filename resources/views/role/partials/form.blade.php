<div class="form-group">
    {!! Form::label('name', 'Nombre del Rol:',) !!}
    {!! Form::text('name', null, ['class' => 'form-control','placeholder' => 'Ingrese el nombre del rol']) !!}
    @error('name')<small class="text-danger">* {{ $message}}</small><br> @enderror
</div>

<h2 class="h3">Listado de permisos</h2>



<div style="display: flex; flex-wrap: wrap; justify-content: flex-start; align-items: flex-start; background-color:;" >
    <div style="background-color:; width: 270px;">
        <h4 class="bg-info text-white p-2">Dashboard</h4>
        @foreach ($permissions as $permission)
            @if (Str::contains(strtolower($permission->description), strtolower('Dashboard')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
            @endif
        @endforeach
    </div>
</div>

<div style="display: flex; flex-wrap: wrap; justify-content: flex-start; align-items: flex-start; background-color:; width: 100%;" >
    <div style="display: flex; flex-wrap: wrap; justify-content: flex-start; align-items: flex-start; background-color:;" >
        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Tickets</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('ticket')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>

        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Cat. categorias</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('categorias')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>
    </div>

    <div style="display: flex; flex-wrap: wrap; justify-content: flex-start; align-items: flex-start; background-color:;" >
        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Activos</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('Activos')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>

        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Cat. tipos</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('tipos')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>

        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Cat. funciones</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('funciones')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>

        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Cat. estados</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('estados')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>
    </div>

    <div style="display: flex; flex-wrap: wrap; justify-content: flex-start; align-items: flex-start; background-color:;" >
        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Usuarios</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('usuarios')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>

        <div style="background-color:; width: 270px;">
            <h4 class="bg-info text-white p-2 m-1">Roles</h4>
            @foreach ($permissions as $permission)
                @if (Str::contains(strtolower($permission->description), strtolower('roles')))
                    <label>
                        {!! Form::checkbox('permissions[]', $permission->id, null, ['class' => 'mr-1 bg-primary ']) !!}
                        {{ $permission->description }}
                    </label><br>
                @endif
            @endforeach
        </div>
    </div>

</div>