{!! Form::hidden('user_id', Auth()->user()->id) !!}
<table style="margin: 0; auto; width:100%">
    <tr>
        <td>
            <table style="margin: 0; auto; width:100%">
                <tr>
                    <td style="width:45%">
                        <div class="form-group" >
                            {!! Form::label('terminal', 'Terminal:') !!}
                            {!! Form::select('terminal', $terminal, null, ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td style="width:10%;">

                    </td>
                    <td style="width:45%">
                        
                    </td>
                </tr>

                <tr>
                    <td style="width:45%">
                        <div class="form-group" >
                            <div class="form-group" >
                                {!! Form::label('descrip', 'Descripción:') !!}
                                {!! Form::text('descrip',null,['class' => 'form-control','placeholder' => 'Ingrese descripción del equipo']) !!}
                                @error('descrip')<small class="text-danger">* {{ $message}}</small><br> @enderror
                            </div>
                        </div>
                    </td>
                    <td style="width:10%;">

                    </td>
                    <td style="width:45%">
                        <div class="form-group">
                            {!! Form::label('status', 'Status:') !!}
                            {!! Form::select('status', ['1'=>'Activo','0' => 'Inactivo'], $status,['class' => 'form-control']) !!}
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:45%">
                        <div class="form-group" style="text-align: left;" >
                            {!! Form::label('serie', 'Serie:') !!}
                            {!! Form::text('serie',null,['class' => 'form-control','placeholder' => 'Ingrese serie del equipo']) !!}
                            @error('serie')<small class="text-danger">* {{ $message}}</small><br> @enderror
                        </div>
                    </td>
                    <td style="width:10%;">

                    </td>
                    <td style="width:45%">
                        <div class="form-group" >
                            {!! Form::label('fecha_adq', 'Fec. Adquisición:') !!}
                            {!! Form::input('date', 'fecha_adq', null, ['class' => 'form-control']) !!}
                            @error('fecha_adq')<small class="text-danger">* {{ $message}}</small><br> @enderror
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:45%">
                        <div class="form-group">
                            {!! Form::label('id_tipo', 'Tipo:') !!}
                            {!! Form::select('id_tipo', $tipos, null, ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td style="width:10%">

                    </td>
                    <td style="width:45%">
                        <div class="form-group" >
                            {!! Form::label('responsable', 'Responsable:') !!}
                            {!! Form::text('responsable',null,['class' => 'form-control','placeholder' => 'Ingrese responsable del equipo']) !!}
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="width:45%">
                        <div class="form-group">
                            {!! Form::label('id_funcion', 'Función:') !!}
                            {!! Form::select('id_funcion', $funciones, null, ['class' => 'form-control']) !!}
                        </div>
                    </td>
                    <td style="width:10%">

                    </td>
                    <td style="width:45%">
                        <div class="form-group">
                            {!! Form::label('id_estado', 'Estado:') !!}
                            {!! Form::select('id_estado', $estados, null, ['class' => 'form-control']) !!}
                        </div>
                    </td>
                </tr>
            </table>


            <table style="margin: 0; auto; width:100%">
                <tr>
                    <td>
                        <div class="form-group">
                            {!! Form::label('caract', 'Caracteristicas:') !!}
                            <div >
                                {!! Form::textarea('caract', null, ['class' => 'form-control','placeholder' => 'Ingrese caracteristicas del equipo']) !!}
                            </div>
                            @error('caract')<small class="text-danger">* {{ $message}}</small><br> @enderror
                        </div>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    
</table>