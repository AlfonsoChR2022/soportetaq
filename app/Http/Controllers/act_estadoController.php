<?php

namespace App\Http\Controllers;
use App\Models\act_estados;
use App\Models\activos;

use Illuminate\Http\Request;

class act_estadoController extends Controller
{
    public function __construct() {
        $this->middleware('can:estado.index')->only('index');
        $this->middleware('can:estado.show')->only('show');
        $this->middleware('can:estado.create')->only('create','store');
        $this->middleware('can:estado.edit')->only('edit','update');
        $this->middleware('can:estado.destroy')->only('destroy');
    }

    public function index(){
        $estds = act_estados::orderBy('id')->paginate(20);
        return view('estado.index',compact('estds'));
    }

    public function create(){
        return view('estado.create');
    }

    public function store(Request $request){
        $request -> validate([
            'estado' => 'required',
        ]);

        $estado = new act_estados();
        $estado -> estado = $request->estado;

        $estado -> save();
        return redirect()->route('estado.show',$estado->id)->with('info','El estado se creó con éxito.');
    }


    public function show(act_estados $estado){
        $estadorium = $estado;
        return view('estado.show',compact('estadorium'));
    }

    public function edit(act_estados $estado){
        $estadorium = $estado;
        return view('estado.edit',compact('estadorium'));
    }


    public function update(Request $request, act_estados $estado){
        $estadorium = $estado;
        $request -> validate([
            'estado' => 'required',
        ]);

        $estadorium -> estado = $request -> estado;
        $estadorium -> save();
        return redirect()->route('estado.show',$estadorium)->with('info','El estado se actualizó con éxito.');
    }

    public function destroy(act_estados $estado){
        $estadorium = $estado;
         $enlace = activos::where('id_estado',$estadorium->id, '<>', '')->get();
        if (count($enlace) === 0){
            $estadorium -> delete();
            return redirect()->route('estado.index')->with('info', 'El estado se eliminó con éxito.');
        }
        return redirect()->route('estado.index')->with('info', 'El estado NO se eliminó, se encuentra asignado en un Activo.');
    }

}

