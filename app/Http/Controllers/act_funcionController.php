<?php

namespace App\Http\Controllers;
use App\Models\act_funciones;
use App\Models\activos;

use Illuminate\Http\Request;

class act_funcionController extends Controller
{
    public function __construct() {
        $this->middleware('can:funcion.index')->only('index');
        $this->middleware('can:funcion.show')->only('show');
        $this->middleware('can:funcion.create')->only('create','store');
        $this->middleware('can:funcion.edit')->only('edit','update');
        $this->middleware('can:funcion.destroy')->only('destroy');
    }

    public function index(){
        $funcns = act_funciones::orderBy('id')->paginate(20);
        return view('funcion.index',compact('funcns'));
    }

    public function create(){
        return view('funcion.create');
    }

    public function store(Request $request){
        $request -> validate([
            'funcion' => 'required',
        ]);

        $funcion = new act_funciones();
        $funcion -> funcion = $request->funcion;

        $funcion -> save();
        return redirect()->route('funcion.show',$funcion->id)->with('info','La función se creó con éxito.');
    }


    public function show(act_funciones $funcion){
        $funcionium = $funcion;
        return view('funcion.show',compact('funcionium'));
    }

    public function edit(act_funciones $funcion){
        $funcionium = $funcion;
        return view('funcion.edit',compact('funcionium'));
    }


    public function update(Request $request, act_funciones $funcion){
        $funcionium = $funcion;
        $request -> validate([
            'funcion' => 'required',
        ]);

        $funcionium -> funcion = $request -> funcion;
        $funcionium -> save();
        return redirect()->route('funcion.show',$funcionium)->with('info','La funcion se actualizó con éxito.');
    }

    public function destroy(act_funciones $funcion){
        $funcionium = $funcion;
         $enlace = activos::where('id_funcion',$funcionium->id, '<>', '')->get();
        if (count($enlace) === 0){
            $funcionium -> delete();
            return redirect()->route('funcion.index')->with('info', 'La funcion se eliminó con éxito.');
        }
        return redirect()->route('funcion.index')->with('info', 'La funcion NO se eliminó, se encuentra asignado en un Activo.');
    }

}
