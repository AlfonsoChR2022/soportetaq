<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\activos;
use App\Models\datempre;

use App\Models\act_estados;
use App\Models\act_tipos;
use App\Models\act_funciones;


class reportesAController extends Controller
{
    public function __construct() {
        $this->middleware('can:reportesA.index')->only('index');
        $this->middleware('can:reportesA.show')->only('show');
    }

    public function index(){
        return view('reportesA.index');
    }

    public function show(Request $request){
        $id_act= $request->reportesA;

        $activo = activos::Where('id',$id_act)->first();

        $terminal = datempre::where('cla_empre',$activo->terminal)->first();
        $tipo =  act_tipos::find($activo->id_tipo)->tipo;
        $funcion = act_funciones::find($activo->id_funcion)->funcion;
        $estado = act_estados::find($activo->id_estado)->estado;

        return view('reportesA.show',compact('activo','terminal','tipo','funcion','estado'));
    }


}

