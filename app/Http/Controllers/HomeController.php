<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tickets;
use App\Models\activos;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $concentrado = tickets::selectRaw('status, COALESCE(count(*), 0) as score')
        ->groupBy('status')
        ->get();

$concentradoA = activos::selectRaw('status, COALESCE(count(*), 0) as score')
        ->groupBy('status')
        ->get();

        return view('home',compact('concentrado','concentradoA'));
    }
}
