<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\act_tipos;
use App\Models\activos;

class act_tipoController extends Controller
{
    public function __construct() {
        $this->middleware('can:tipo.index')->only('index');
        $this->middleware('can:tipo.show')->only('show');
        $this->middleware('can:tipo.create')->only('create','store');
        $this->middleware('can:tipo.edit')->only('edit','update');
        $this->middleware('can:tipo.destroy')->only('destroy');
    }

    public function index(){
        $tips = act_tipos::orderBy('id')->paginate(20);
        return view('tipo.index',compact('tips'));
    }

    public function create(){
        return view('tipo.create');
    }

    public function store(Request $request){
        $request -> validate([
            'tipo' => 'required',
        ]);

        $tipo = new act_tipos();
        $tipo -> tipo = $request->tipo;

        $tipo -> save();
        return redirect()->route('tipo.show',$tipo->id)->with('info','El tipo se creó con éxito.');
    }


    public function show(act_tipos $tipo){
        $tiporium = $tipo;
        return view('tipo.show',compact('tiporium'));
    }

    public function edit(act_tipos $tipo){
        $tiporium = $tipo;
        return view('tipo.edit',compact('tiporium'));
    }


    public function update(Request $request, act_tipos $tipo){
        $tiporium = $tipo;
        $request -> validate([
            'tipo' => 'required',
        ]);

        $tiporium -> tipo = $request -> tipo;
        $tiporium -> save();
        return redirect()->route('tipo.show',$tiporium)->with('info','El tipo se actualizó con éxito.');
    }

    public function destroy(act_tipos $tipo){
        $tiporium = $tipo;
         $enlace = activos::where('id_tipo',$tiporium->id, '<>', '')->get();
        if (count($enlace) === 0){
            $tiporium -> delete();
            return redirect()->route('tipo.index')->with('info', 'El tipo se eliminó con éxito.');
        }
        return redirect()->route('tipo.index')->with('info', 'El tipo NO se eliminó, se encuentra asignado en un Activo.');
    }

}
