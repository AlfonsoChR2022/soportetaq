<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\activos;
use App\Models\datempre;

use App\Models\act_estados;
use App\Models\act_tipos;
use App\Models\act_funciones;


class activoController extends Controller
{
    public function __construct() {
        $this->middleware('can:activo.index')->only('index');
        $this->middleware('can:activo.show')->only('show');
        $this->middleware('can:activo.create')->only('create','store');
        $this->middleware('can:activo.edit')->only('edit','update');
        $this->middleware('can:activo.destroy')->only('destroy');
        $this->middleware('can:activo.procesar')->only('procesar');
    }

    public function procesar(Request $request)
    {
        $statusFiltro = $request->input('statusFiltro');
        return view('activo.index',compact('statusFiltro'));
    }


    public function index(Request $request){
        $actvs = activos::orderByDesc('id')
                    ->paginate(20);

        $t_estado = act_estados::all();
        $t_tipo = act_tipos::all();
        $t_funcion = act_funciones::all();

        $statusFiltro='';
        return view('activo.index',compact('actvs','t_estado','t_tipo','t_funcion','statusFiltro'));
    }

    public function create(){
        $terminal = datempre::selectRaw("concat('(',cla_empre, ') ', descrip) as descrip, cla_empre")->pluck('descrip', 'cla_empre');
        $estados = act_estados::pluck('estado','id');
        $tipos = act_tipos::pluck('tipo','id');
        $funciones = act_funciones::pluck('funcion','id');
        $status=1;

        return view('activo.create',compact('terminal','estados','tipos','funciones','status'));
    }

    public function store(Request $request){
        $request -> validate([
            'descrip' => 'required',
            'serie' => 'required',
            'caract' => 'required',
            'fecha_adq' => 'required',
        ]);

        $activo = new activos();
        //$activo -> terminal = datempre::first()->cla_empre;
        $activo -> terminal = $request -> terminal;

        $activo -> descrip = $request -> descrip;
        $activo -> caract = $request -> caract;

        $activo -> id_tipo = $request -> id_tipo;
        $activo -> id_funcion = $request -> id_funcion;
        $activo -> id_estado = $request -> id_estado;

        $activo -> serie = $request -> serie;
        $activo -> fecha_adq = $request -> fecha_adq;
        $activo -> responsable = $request -> responsable;
        $activo -> status = $request -> status;

        $activo -> save();
        return redirect()->route('activo.show',$activo->id)->with('info','El activo se creó con éxito.');
    }


    public function update(Request $request, activos $activo){
        if (isset($_POST['btn-update'])) {
            $request -> validate([
                'descrip' => 'required',
                'serie' => 'required',
                'caract' => 'required',
                'fecha_adq' => 'required',
            ]);

        // UPDATE
        $activo -> terminal = $request -> terminal;

        $activo -> descrip = $request -> descrip;
        $activo -> caract = $request -> caract;

        $activo -> id_tipo = $request -> id_tipo;
        $activo -> id_funcion = $request -> id_funcion;
        $activo -> id_estado = $request -> id_estado;

        $activo -> serie = $request -> serie;
        $activo -> fecha_adq = $request -> fecha_adq;
        $activo -> responsable = $request -> responsable;
        $activo -> status = $request -> status;
        

            $activo -> save();
            return redirect()->route('activo.show',$activo)->with('info','Se actualizaron datos del activo correctamente.');
        }
    }


    public function show(activos $activo){
        $estado = act_estados::find($activo->id_estado)->estado;
        $tipo = act_tipos::find($activo->id_tipo)->tipo;
        $funcion = act_funciones::find($activo->id_funcion)->funcion;
        $terminal = datempre::where('cla_empre',$activo->terminal)->first();

        return view('activo.show',compact('activo','estado','tipo','funcion','terminal'));
    }

    public function edit(activos $activo){
        $terminal = datempre::selectRaw("concat('(',cla_empre, ') ', descrip) as descrip, cla_empre")->pluck('descrip', 'cla_empre');

        $tipos = act_tipos::pluck('tipo','id');
        $funciones = act_funciones::pluck('funcion','id');
        $estados = act_estados::pluck('estado','id');


        $status =  $activo->status;

        return view('activo.edit',compact('activo','terminal','tipos','funciones','estados','status'));
    }

    public function destroy(activos $activo){
        $activo -> delete();
        return redirect()->route('activo.index')->with('info', 'El activo se eliminó con éxito.');
    }
}
