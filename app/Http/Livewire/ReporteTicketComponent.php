<?php

namespace App\Http\Livewire;
use App\Models\tickets;
use App\Models\datempre;
use App\Models\categorias;
use App\Models\user;

use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;


class ReporteTicketComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $terminalFiltro;
    public $categoriaFiltro;
    public $prioridadFiltro;


    public $sort = 'id';
    public $direction = 'desc';

    public function updatingSearch(){
        $this->resetPage();
    }
    public function updatingTerminalFiltro(){
        $this->resetPage();
    }
    public function updatingCategoriaFiltro(){
        $this->resetPage();
    }
    public function updatingPrioridadFiltro(){
        $this->resetPage();
    }

    public function render(){
        $tkts = tickets::where('evento', 'like','%'  . $this->search . '%')
                ->Where('terminal','like','%'  . $this->terminalFiltro . '%')
                ->Where('id_categoria','like','%'  . $this->categoriaFiltro . '%')
                ->Where('prioridad','like','%'  . $this->prioridadFiltro . '%')
                ->orderBy($this->sort, $this->direction)
                ->Where('status','3')
                ->paginate(20);

        $terminales = datempre::all();
        $t_evto = categorias::all();
        $userX = user::all();
    return view('livewire.reporte-ticket-component',compact('tkts','t_evto','terminales','userX'));
    }

    public function order($sort){
        // $this->sort = $sort;
        if($this->sort == $sort){
            if($this->direction == 'desc'){
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }

        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function seleccionarPrimerElemento()
    {
        $this->search =  null;
        $this->terminalFiltro = null;
        $this->categoriaFiltro =  null;
        $this->prioridadFiltro =  null;

    }


}
