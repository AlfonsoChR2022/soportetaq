<?php

namespace App\Http\Livewire;
use App\Models\tickets;
use App\Models\categorias;
use App\Models\datempre;
use App\Models\User;


use Livewire\Component;
use Livewire\WithPagination;


class ListadoTicketComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $sort = 'id';
    public $direction = 'desc';

    public $terminalFiltro;
    public $categoriaFiltro;
    public $statusFiltro;
    public $prioridadFiltro;


    public function updatingSearch(){
        $this->resetPage();
    }
    public function updatingTerminalFiltro(){
        $this->resetPage();
    }
    public function updatingCategoriaFiltro(){
        $this->resetPage();
    }
    public function updatingStatusFiltro(){
        $this->resetPage();
    }
    public function updatingPrioridadFiltro(){
        $this->resetPage();
    }


    public function render(){
        $tkts = tickets::where('evento', 'like','%'  . $this->search . '%')
                        ->Where('terminal','like','%'  . $this->terminalFiltro . '%')
                        ->Where('id_categoria','like','%'  . $this->categoriaFiltro . '%')
                        ->Where('status','like','%'  . $this->statusFiltro . '%')
                        ->Where('prioridad','like','%'  . $this->prioridadFiltro . '%')
                        ->orderBy('id','desc')
                        ->paginate(20);

        $terminales = datempre::all();
        $t_evto = categorias::all();
        $userX = user::all();
    return view('livewire.listado-ticket-component',compact('tkts','t_evto','terminales','userX'));
    }

    public function seleccionarPrimerElemento()
    {
        $this->search =  null;
        $this->terminalFiltro = null;
        $this->categoriaFiltro =  null;
        $this->prioridadFiltro =  null;
        $this->statusFiltro =  null;

    }

}