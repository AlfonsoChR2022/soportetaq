<?php

namespace App\Http\Livewire\Activos;

use App\Models\activos;
use App\Models\datempre;
use App\Models\act_estados;
use App\Models\act_tipos;
use App\Models\act_funciones;

use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithPagination;


use App\Models\tickets;


class ReporteActivoComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $terminalFiltro;
    
    public $estadoFiltro;
    public $tipoFiltro;
    public $funcionFiltro;

    public $sort = 'id';
    public $direction = 'desc';

    public function updatingSearch(){
        $this->resetPage();
    }
    public function updatingTerminalFiltro(){
        $this->resetPage();
    }
    public function updatingestadoFiltro(){
        $this->resetPage();
    }
    public function updatingtipoFiltro(){
        $this->resetPage();
    }
    public function updatingfuncionFiltro(){
        $this->resetPage();
    }

    public function render(){
        $activos = activos::where('descrip', 'like','%'  . $this->search . '%')
        ->Where('terminal','like','%'  . $this->terminalFiltro . '%')
        ->Where('id_estado','like','%'  . $this->estadoFiltro . '%')
        ->Where('id_tipo','like','%'  . $this->tipoFiltro . '%')
        ->Where('id_funcion','like','%'  . $this->funcionFiltro . '%')
        ->Where('status','1')
        ->orderBy('id','desc')
        ->paginate(20);

    $terminales = datempre::all();
    $t_tipo = act_tipos::all();
    $t_funcion = act_funciones::all();
    $t_estado = act_estados::all();
    return view('livewire.activos.reporte-activo-component',compact('activos','terminales','t_tipo','t_funcion','t_estado'));
}
    
    public function order($sort){
        // $this->sort = $sort;
        if($this->sort == $sort){
            if($this->direction == 'desc'){
                $this->direction = 'asc';
            } else {
                $this->direction = 'desc';
            }

        } else {
            $this->sort = $sort;
            $this->direction = 'asc';
        }
    }

    public function seleccionarPrimerElemento()
    {
        $this->search =  null;
        $this->terminalFiltro = null;
        $this-> estadoFiltro= null;
        $this-> tipoFiltro= null;
        $this-> funcionFiltro= null;

    }


}
