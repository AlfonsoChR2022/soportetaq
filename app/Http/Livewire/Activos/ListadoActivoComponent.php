<?php

namespace App\Http\Livewire\Activos;

use Livewire\Component;
use Livewire\WithPagination;

use App\Models\activos;
use App\Models\datempre;
use App\Models\act_estados;
use App\Models\act_tipos;
use App\Models\act_funciones;

class ListadoActivoComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;
    public $sort = 'id';
    public $direction = 'desc';

    public $terminalFiltro;
    public $estadoFiltro;
    public $tipoFiltro;
    public $funcionFiltro;
    public $statusFiltro;


    public function updatingSearch(){
        $this->resetPage();
    }
    public function updatingTerminalFiltro(){
        $this->resetPage();
    }
    public function updatingstadoFiltro(){
        $this->resetPage();
    }
    public function updatingtipoFiltro(){
        $this->resetPage();
    }
    public function updatingfuncionFiltro(){
        $this->resetPage();
    }
    public function updatingstatusFiltro(){
        $this->resetPage();
    }


    public function render(){
        $activos = activos::where('descrip', 'like','%'  . $this->search . '%')
                        ->Where('terminal','like','%'  . $this->terminalFiltro . '%')
                        ->Where('id_estado','like','%'  . $this->estadoFiltro . '%')
                        ->Where('id_tipo','like','%'  . $this->tipoFiltro . '%')
                        ->Where('id_funcion','like','%'  . $this->funcionFiltro . '%')
                        ->Where('status','like','%'  . $this->statusFiltro . '%')
                        ->orderBy('id','desc')
                        ->paginate(20);

        $terminales = datempre::all();
        $t_estado = act_estados::all();
        $t_tipo = act_tipos::all();
        $t_funcion = act_funciones::all();

    return view('livewire.activos.listado-activo-component',compact('activos','t_estado','t_tipo','t_funcion','terminales'));
    //activos.listado-activo-component
    }

    public function seleccionarPrimerElemento()
    {
        $this->search =  null;
        $this->terminalFiltro = null;
        $this->estadoFiltro =  null;
        $this->funcionFiltro =  null;
        $this->tipoFiltro =  null;
        $this->statusFiltro =  null;

    }

}
